CXX = g++
CLANG = clang

NAME = n-puzzle

CFLAGS = -Wall -Werror -Wextra -std=c++11

INCLUDE = -I includes/

LIB_FLAG = -lncurses

HEAD = 	includes/main.hpp \
		includes/map.hpp \
		includes/event.class.hpp \
		includes/ft42.class.hpp \
		includes/ncurse.class.hpp \
		includes/calc.class.hpp \
		includes/string_ncurse.h \
		includes/Parse.class.hpp

SRC = 	src/main.cpp \
		src/ncurse.class.cpp \
		src/event.class.cpp \
		src/calc.class.cpp \
		src/ft42.class.cpp \
		src/Parse.class.cpp

OBJ = $(SRC:.cpp=.cpp.o)

%.cpp.o: %.cpp $(HEAD)
	clear
	@$(CXX) $(CFLAGS) $(INCLUDE) $(CFLAGS) -c $< -o $@

all: $(NAME)

$(NAME): $(OBJ)
	$(CXX) $(CFLAGS) $(OBJ) -o $(NAME) $(LIB_FLAG)

clean:
	@rm -rf $(OBJ)

fclean: clean
	@rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
