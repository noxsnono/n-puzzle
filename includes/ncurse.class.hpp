#ifndef GRAPHIC_HPP
# define GRAPHIC_HPP

# include <main.hpp>
# include <cstdlib>
# include <stdlib.h>
# include <ft42.class.hpp>
# include <event.class.hpp>
# include <string_ncurse.h>
# include <ctime>
# include <math.h>

class 									Event;

typedef struct							s_key {
	int									key;
	struct s_key *						next;
}										t_key;

class Graphic : public ft42 {
private:
	WINDOW *							_window;
	int									_size;
	std::vector<int> *					_key;
	Map	*								_map;
	Map *								_originalMap;
	int 								screenY;
	int 								screenX;
	int 								allCaseSize;
	std::list<std::string> *			_waySolution;
	std::list<std::string>::iterator	_wayBegin;
	std::list<std::string>::iterator	_wayEnd;
	std::list<std::string>::iterator	_itWaySolution;
	time_t								_time_start;
	time_t								_time_end;
	bool								FirstLaunch;


	Graphic( void );
	
public:
	Event *								mainEvent;

	Graphic( Event * event );
	Graphic( Graphic const & src );
	Graphic & operator=( Graphic const & rhs );
	~Graphic( void );

	void					keyboard( void );
	void					init( int size, Map * map );
	void					close( void );
	void					render_scene( void );
	std::vector<int> **		get_touch_list( void );
	void					draw_square( int IncY, int IncX, int nbr );
	void					draw_border_square(int IncY, int IncX);
	void					draw_legend( void );
	void 					draw_void( int IncY, int IncX);
	void 					change_mode_way_solution( int mode );
	void 					setMap( Map * map );
	void 					setWaySolution( void );
};

#endif
