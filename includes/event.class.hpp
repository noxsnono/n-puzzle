#ifndef EVENT_CLASS_HPP
# define EVENT_CLASS_HPP

# include <main.hpp>
# include <ft42.class.hpp>
# include <Parse.class.hpp>

class Graphic;
class Calc;
class Parse;

class Event : public ft42 {
private:
	Map		*_map;
	int		_size;
	Graphic *_graphic;
	Calc	*_calc;
	Parse 	*_parse;

public:
	bool	running;
	bool	solution;

	Event( void );
	Event( Event const & src );
	Event & operator=( Event const & rhs );
	~Event( void );

	void	init( int ac, char **av );
	void	exit_free( void );
	void	init_map( void );
	void	random_gen( void );
	void	test_gen( void );	// SUPPR
	void	run( void );
	void	setMap( Map * newMap );
	Calc *	get_calc( void );
	void	set_new_calc( void );
};

#endif
