#ifndef STRING_NCURSE_H
# define STRING_NCURSE_H

# define NO_DATA "0x0 NO SET"
# define VOID_STRING "                                 "

# define LEGEND_SPACE "Key 'SPACE' 'A' for next step"
# define LEGEND_SPACE_Y 23

# define LEGEND_DELETE "Key 'DELETE' 'D' or previous step"
# define LEGEND_DELETE_Y 21

# define LEGEND_QUIT "Key 'ECHAP' 'Q' 'E' to exit program"
# define LEGEND_QUIT_Y 19

# define MODE_MANATHAN "Key '1' MANATHAN resolution"
# define MODE_MANATHAN_Y 17

# define MODE_GOOD_CASES "Key '2' Resolution using number of good cases"
# define MODE_GOOD_CASES_Y 15

# define MODE_WRONG_CASES "Key '3' Resolution using number of wrong cases"
# define MODE_WRONG_CASES_Y 13

# define MODE_BOUNDED_CASES "Key '4' Resolution using 'Bounded Relaxation'"
# define MODE_BOUNDED_CASES_Y 11

# define TIME "Resolution's time in seconds:"
# define TIME_Y 9

# define TOTAL_OPEN_WAY "Total Ways opened:"
# define TOTAL_OPEN_WAY_Y 7

# define JUMP_TO_SOLUTION "Jump to solution:"
# define JUMP_TO_SOLUTION_Y 5

# define SOLUTION_YES "Possible Solution"
# define SOLUTION_NO "Impossible Solution"
# define SOLUTION_Y 1

#endif
