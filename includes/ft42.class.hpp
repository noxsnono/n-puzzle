#ifndef FT42_CLASS_HPP
# define FT42_CLASS_HPP

# include <cstdlib>
# include <cstring>
# include <exception>
# include <fstream>
# include <iostream>
# include <istream>
# include <sstream>
# include <string>
# include <vector>
# include <regex>
# include <unistd.h>
# include <stdlib.h>
# include <set>
# include <map>

# define LOG_PATH	"log/debugg.log"

typedef std::map<int, std::vector<int>> Map;
class ft42 {
public:
	

	ft42();
	virtual ~ft42();

	void					w_log( std::string const newEntry );
	void					w_full( std::string const newEntry );
	void					w_error( std::string const newEntry );
	std::string				logTime( std::string const & sentence );
	std::string	*			serialize( Map *map, int size );
	Map *					toTab( std::string const * inc, int size );


	std::ofstream &			getLogFD( void );

private:
	static std::string		logFileName;
	static std::ofstream	lodFD;
	std::string				result;
	time_t					timer;
	struct tm *				timeinfo;
	char					buffer[80];
};

#endif