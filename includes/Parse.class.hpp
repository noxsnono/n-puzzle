#ifndef PARSE_CLASS_HPP
# define PARSE_CLASS_HPP

# include <main.hpp>
# include <ft42.class.hpp>
# include <iostream>
# include <fstream>
# include <sstream>
# include <cctype>
# include <stdlib.h>

class Parse : public ft42 {
public:
	Parse( char ** av );
	~Parse();

	void	sizeLine( std::string const &	line );
	void	tabLine( std::string const & line );

	Map *	getMap( void );
	int		getSize( void );

private:
	Map *			_map;
	std::ifstream	_lodFD;
	int				_size;
	std::list<int>	_nbr;
	int				_mapY;

	Parse();
};

#endif
