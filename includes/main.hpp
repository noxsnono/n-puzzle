#ifndef MAIN_HPP
# define MAIN_HPP

# include <ncurses.h>
# include <vector>
# include <list>
# include <iostream>
# include <stdexcept>
# include <string.h>
# include <stdlib.h>
# include <time.h>

# include <ncurse.class.hpp>
# include <event.class.hpp>
# include <calc.class.hpp>
# include <Parse.class.hpp>
# include <map.hpp>


# define ERR_FORMAT "./n-puzzle [size]"

# define MAX_SIZE 9
# define MIN_SIZE 3
# define DEFAULT_SIZE 3
# define CASE_SIZE_Y 5
# define CASE_SIZE_X 8
# define UP		65
# define DOWN	66
# define LEFT	68
# define RIGHT	67
# define ECHAP	27
# define ONE	49
# define TWO	50
# define THREE	51
# define SPACE	32

class Event;

typedef struct s_data {
	Event *		event;
	Graphic *	graphic;
	Calc *		calc;
}t_data;

t_data *	get_data(void);

#endif
