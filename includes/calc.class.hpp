#ifndef CALC_CLASS_HPP
# define CALC_CLASS_HPP

# include <main.hpp>
# include <ft42.class.hpp>
# include <string_ncurse.h>
# include <iterator>
# include <set>
# include <list>
# include <iostream>
# include <string>
# include <array>


typedef struct 		s_tab {
	Map				*tab;
	std::string 	*hash;
	bool			use;
}					t_tab;

typedef struct 		s_open {
	Map				*tab;
	std::string 	*hash;
	int 			manhattan;
	int 			score;
}					t_open;

typedef struct 		s_closed {
	Map				*tab;
	std::string 	*hash;
	int 			manhattan;
	int 			score;
}					t_closed;

class Calc : public ft42 {
private:
	std::list<t_open *>	*					_opened;
	std::set<t_closed *> *					_closed;
	std::map<std::string, std::string> *	_from;
	std::list<std::string> *				_waySolution;
	std::list<int>							_finalPts;
	int										_size;
	t_tab									_test[4];
	int										_available;
	Map *									_actual_state;
	Map	*									_map_final;
	time_t									_time;
	int 									_calc_mode;	// 0 manhattan, 1 wrong tile, 2 good tile, 3 a* opti

public:
	bool									possibleSolution;
	int										totalWayOpen;

	Calc( void );
	Calc( std::string map, int size );
	Calc( Calc const & src );
	Calc & operator=( Calc const & rhs );
	~Calc( void );

	int 						next_move( void );
	void						next_state( void );
	void						copy_map(int index);
	void						do_calc( int score, std::string actual_state );
	int							manhattan(Map *tab);
	int							wrong_tile(Map *tab);
	int							good_tile(Map *tab);
	int							a_bounded(Map *tab);
	Map *						new_map( void );
	t_closed *					new_closed( void );
	t_open *					new_open( void );
	void						set_final_map( void );
	void						init_actual_state( Map * map );
	bool						is_done(t_open * state);
	void						get_pos(int find, int *x, int *y);
	void						change_zero_pos(int i, int y, int x, int y2, int x2);
	bool						is_close(std::string state);
	bool						check_open(std::string state, int score, std::string old);
	bool						check_closed(std::string state, int score);
	bool						close_bad_state( void );
	void						add_from_list(std::string state, std::string old);
	void						fill_solution(void);
	bool						check_time( void );
	void						print_debug_map(Map *tab);
	void						print_debug_map_to_log(Map *tab);
	void						change_calc_mode(int mode);
	bool						run_calc( void );
	std::list<std::string> *	getWaySolution( void );
	void						delete_Solution(void);
};

#endif
