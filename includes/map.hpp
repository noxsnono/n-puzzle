#ifndef MAP_HPP
# define MAP_HPP

# include <string>

static const std::string	g_M3 = "\"#$)!%('&";
static const std::string	g_M4 = "\"#$%-./&,!0'+*)(";
static const std::string	g_M5 = "\"#$%&1234'09!5(/876).-,+*";
static const std::string	g_M6 = "\"#$%&'56789(4ABC:)3@!D;*2?>=<+10/.-,";
static const std::string	g_M7 = "\"#$%&'(9:;<=>)8IJKL?*7HQ!M@+6GPONA,5FEDCB-43210/.";
static const std::string	g_M8 = "\"#$%&'()=>?@ABC*<QRSTUD+;P]^_VE,:O\\!`WF-9N[ZYXG.8MLKJIH/76543210";
static const std::string	g_M9 = "\"#$%&'()*ABCDEFGH+@YZ[\\]^I,?Xijkl_J->Whq!m`K.=VgponaL/<UfedcbM0;TSRQPON1:98765432";

static const std::string	g_select[7] = { g_M3, g_M4, g_M5, g_M6, g_M7, g_M8, g_M9 };

#endif
