#include <event.class.hpp>

Event::Event( void ) : 	_size(DEFAULT_SIZE),
						running(true),
						solution(true) {

		t_data *	d = get_data();

		if (NULL == d) {
			this->w_error("Assign Event prt error.");
			throw std::exception();
		}
		d->event = this;
}

Event::Event( Event const & src ) { *this = src; }

Event & Event::operator=( Event const & rhs ) {
	if (this != &rhs) {}
	return (*this);
}
Event::~Event( void ) {}

void	Event::setMap( Map * newMap ) { this->_map = newMap; }

void	Event::init( int ac, char **av ) {
	this->_graphic = new Graphic(this);

	if (ac == 3) {
		this->_size = atoi(av[2]);
		init_map();
		srand(time(NULL));
		random_gen();
	}
	else {
		this->_parse = new Parse(av);
		if (NULL == this->_parse) {
			this->w_error("Event::init Memory Allocation Parse error.");
			throw std::exception();
		}
		this->_map = this->_parse->getMap();
		this->_size = this->_parse->getSize();
	}
	this->_calc = new Calc(*serialize(this->_map, this->_size), this->_size);
	this->_graphic->init(this->_size, this->_map);
}

void	Event::exit_free( void ) {	// free here
	this->_graphic->close();
	exit(0);
}

void	Event::init_map( void ) {
	this->_map = new Map();
	int i, y = 0;
	std::vector<int> line;

	while (y < this->_size) {
		i = 0;
		while (i < this->_size) {
			line.push_back(0);
			i++;
		}
		this->_map->insert(std::pair<int,std::vector<int>>(y,line));
		line.clear();
		y++;
	}
}

void	Event::test_gen( void ) {
	std::vector<int>	*		dest;

	dest = &this->_map->find(0)->second;
	(*dest)[0] = 4;
	(*dest)[1] = 15;
	(*dest)[2] = 1;
	(*dest)[3] = 2;

	dest = &this->_map->find(1)->second;
	(*dest)[0] = 0;
	(*dest)[1] = 14;
	(*dest)[2] = 8;
	(*dest)[3] = 13;

	dest = &this->_map->find(2)->second;
	(*dest)[0] = 10;
	(*dest)[1] = 12;
	(*dest)[2] = 3;
	(*dest)[3] = 9;

	dest = &this->_map->find(3)->second;
	(*dest)[0] = 11;
	(*dest)[1] = 5;
	(*dest)[2] = 7;
	(*dest)[3] = 6;
}

void	Event::random_gen( void ) {
	int i, y = 0;
	std::vector<int> *vect;
	std::list<int>	list;
	std::list<int>::iterator it;

	for (int x = 0; x < (this->_size * this->_size); ++x) {
		list.push_back(x);
	}
	while (y < this->_size) {
		i = 0;
		vect = &this->_map->find(y)->second;
		while (i < this->_size) {
			if (list.size() > 0) {
				it = list.begin();
				advance(it, rand() % list.size());
				(*vect)[i] = *it;
				list.erase(it);
			}
			i++;
		}
		y++;
	}
	list.clear();
}

void	Event::run( void ) {
	while (this->running == true) {
		this->_graphic->render_scene();
		usleep(200000);
	}
	this->_graphic->close();
}

Calc *	Event::get_calc( void ) { return (this->_calc); }

void	Event::set_new_calc( void ) {
	this->_calc = new Calc(*serialize(this->_map, this->_size), this->_size);
	if (this->_calc == NULL) {
		this->w_error("Event::set_new_calc Memory Allocation error.");
		throw std::exception();
	}
}
