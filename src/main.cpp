#include <main.hpp>

int main( int ac, char **av ) {
	if (!(ac == 3 && strcmp(av[1], "-r") == 0 && atoi(av[2]) >= 3)
		&& !(ac == 2 && strchr(av[1], '.') != NULL)) {
		std::cerr << "./n-puzzle <[map] | [-r size]" << std::endl;
		return (EXIT_FAILURE);
	}
	t_data *					d = get_data();
	if (NULL == d) {
		std::cerr << "Init Error." << std::endl;
		return (EXIT_FAILURE);
	}
	try {
		Event *					event = new Event();

		event->init(ac, av);
		event->run();
		event->w_error("EXIT MAIN SUCCESS");
	}
	catch (std::exception & e) {
		std::cerr << "EXIT FAILURE" << std::endl;
		return (EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);
}

t_data *	get_data(void) {
	static t_data *	d = NULL;

	if (NULL == d) {
		d = static_cast<t_data *>( std::malloc( sizeof(t_data) ) );
		if (NULL == d) {
			std::cerr << "Data Initialisation error> try to free memory and try again." << std::endl;
			return NULL;
		}
	}
	return d;
}
