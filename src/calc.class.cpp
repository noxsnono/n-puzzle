#include <calc.class.hpp>

Calc::Calc( void ) {}

std::list<std::string> *	Calc::getWaySolution ( void ) { return this->_waySolution; }

Calc::Calc( std::string map, int size ) : _size(size), _calc_mode(0), possibleSolution(false), totalWayOpen(0) {
	t_open *state = NULL;

	this->_opened = new std::list<t_open *>();
	this->_closed = new std::set<t_closed *>();
	this->_from = new std::map<std::string, std::string>();
	set_final_map();

	this->_test[0].tab = new_map();
	this->_test[1].tab = new_map();
	this->_test[2].tab = new_map();
	this->_test[3].tab = new_map();
	state = new_open();
	this->_actual_state = new_map();
	state->hash =  new std::string(map);
	state->tab = toTab(&map, this->_size);
	state->manhattan = manhattan(state->tab);
	state->score = 0;
	this->_time = time(NULL);
	this->_opened->push_back(state);
}
 	
Calc::Calc( Calc const & src ) { *this = src; }

Calc & Calc::operator=( Calc const & rhs ) {
	if (this != &rhs) {}
	return (*this);
}

Calc::~Calc( void ) {}

void	Calc::set_final_map( void ) {
	int 				y = 0,
						x = 0;
	std::vector<int> 	vect;

	this->_map_final = this->toTab(&g_select[this->_size -3], this->_size);
	while (y < this->_size) {
		x = 0;
		vect = this->_map_final->find(y)->second;
		while (x < this->_size) {
			this->_finalPts.push_back(vect[x]);
			x++;
		}
		y++;
	}
}

int		Calc::wrong_tile(Map *tab) {
	int x, y = 0, tmp = 0, value = 0;
	std::list<int>::iterator	it;
	std::vector<int>			vect;

	while (y < this->_size) {
		x = 0;
		vect = tab->find(y)->second;
		while (x < this->_size && vect[x] != 0) {
			it = std::find(this->_finalPts.begin(), this->_finalPts.end(), vect[x]);
			tmp = std::distance(this->_finalPts.begin(), it);
			if (tmp != 0)
				value++;
			x++;
		}
		y++;
	}
	return (value);
}

int		Calc::good_tile(Map *tab) {
	int x, y = 0, tmp = 0, value = 0;
	std::list<int>::iterator	it;
	std::vector<int>			vect;

	while (y < this->_size) {
		x = 0;
		vect = tab->find(y)->second;
		while (x < this->_size && vect[x] != 0) {
			it = std::find(this->_finalPts.begin(), this->_finalPts.end(), vect[x]);
			tmp = std::distance(this->_finalPts.begin(), it);
			if (tmp == 0)
				value++;
			x++;
		}
		y++;
	}
	return (value);
}

int		Calc::manhattan(Map *tab) {
	int x, y = 0, tmp = 0, value = 0;
	std::list<int>::iterator	it;
	std::vector<int>			vect;

	while (y < this->_size) {
		x = 0;
		vect = tab->find(y)->second;
		while (x < this->_size) {
			it = std::find(this->_finalPts.begin(), this->_finalPts.end(), vect[x]);
			tmp = std::distance(this->_finalPts.begin(), it);
			value += abs(x - (tmp % this->_size)) + abs(y - (tmp / this->_size));
			// std::cout << value <<  std::endl;
			x++;
		}
		y++;
	}
	return (value);
}

int		Calc::a_bounded(Map *tab) {
	int epsilon = 1;

	return ((1 + epsilon) * manhattan(tab));
}

t_open * Calc::new_open( void ) {
	t_open *state = NULL;

	state = static_cast<t_open *>(std::malloc(sizeof(t_open)));
	if (state == NULL) {
		this->w_error("Calc::Calc Memory fail.");
		throw std::exception();
	}
	return (state);
}

t_closed * Calc::new_closed( void ) {
	t_closed *state = NULL;

	state = static_cast<t_closed *>(std::malloc(sizeof(t_closed)));
	if (state == NULL) {
		this->w_error("Calc::Calc Memory fail.");
		throw std::exception();
	}
	return (state);
}

Map *	Calc::new_map( void ) {
	Map * map = new Map();
	int i, y = 0;
	std::vector<int> line;

	while (y < this->_size) {
		i = 0;
		while (i < this->_size) {
			line.push_back(0);
			i++;
		}
		map->insert(std::pair<int,std::vector<int>>(y,line));
		line.clear();
		y++;
	}
	return (map);
}

void	Calc::init_actual_state( Map * map ) {
	int x, y = 0;
	std::vector<int>			src;
	std::vector<int>	*		dest;

	while (y < this->_size) {
		x = 0;
		src = map->find(y)->second;
		dest = &_actual_state->find(y)->second;
		while (x < this->_size) {
			(*dest)[x] = src[x];
			x++;
		}
		y++;
	}
}

void	Calc::copy_map(int index) {
	int x, y = 0;
	std::vector<int>			src;
	std::vector<int>	*		dest;

	while (y < this->_size) {
		x = 0;
		src = this->_actual_state->find(y)->second;
		dest = &this->_test[index].tab->find(y)->second;
		while (x < this->_size) {
			(*dest)[x] = src[x];
			x++;
		}
		y++;
	}
}

bool	Calc::is_done(t_open * state) {
	if (g_select[this->_size - 3].compare(*state->hash) != 0) // not the end
		return (false);
	else
		return (true);
}

void	Calc::get_pos(int find, int *x, int *y) {
	std::vector<int>	vect;

	while (*y < this->_size) {
			*x = 0;
			vect = this->_actual_state->find(*y)->second;
			while (*x < this->_size) {
				if (vect[*x] == find) {
					return ;
				}
				(*x)++;
			}
			(*y)++;
		}
}

void	Calc::add_from_list(std::string state, std::string old) {
	this->_from->insert(std::pair<std::string,std::string>(state, old));
}

void	Calc::change_calc_mode(int mode) { this->_calc_mode = mode; }

void	Calc::do_calc( int score, std::string old) {
	int	i = 0;
	t_open * state = NULL;

	while (i < 4) {
		if (this->_test[i].use == true) {
			if (check_open(*this->_test[i].hash, score, old) == false) {
				state = new_open();
				state->tab = this->toTab(this->_test[i].hash, this->_size);
				state->hash = new std::string(*this->_test[i].hash);
				if (this->_calc_mode == 0) // manhattan
					state->manhattan = manhattan(state->tab);
				else if (this->_calc_mode == 1)	// wrong tile
					state->manhattan = wrong_tile(state->tab);
				else if (this->_calc_mode == 2)	// good tile
					state->manhattan = good_tile(state->tab);
				else	// a* bounded relaxation
					state->manhattan = a_bounded(state->tab);

				state->score = score;
				this->_opened->push_back(state);	// must check for leaks
				add_from_list(*state->hash, old);
			}
		}
		i++;
	}
}

void	Calc::change_zero_pos(int i, int y, int x, int y2, int x2) {
	std::vector<int>	*one;
	std::vector<int>	*two;
	int tmp;

	one = &this->_test[i].tab->find(y)->second;
	two = &this->_test[i].tab->find(y2)->second;
	tmp = (*one)[x];
	(*one)[x] = (*two)[x2];
	(*two)[x2] = tmp;
}

bool	Calc::is_close(std::string state) {
	std::set<t_closed *>::iterator it = this->_closed->begin();
	std::set<t_closed *>::iterator end = this->_closed->end();

	if (this->_closed->size() == 0) {
		return (false);
	}
	while (it != end) {
		if ((*it)->hash->compare(state) == 0)
			return (true);
		it++;
	}
	return (false);
}

bool	Calc::check_closed(std::string state, int score) {
	std::set<t_closed *>::iterator	it = this->_closed->begin();
	std::set<t_closed *>::iterator	end = this->_closed->end();
	t_open * 						open = NULL;

	while (it != end) {
		if ((*it)->hash->compare(state) == 0 && score < (*it)->score) {
			(*it)->score = score;
			open = new_open();
			open->hash = new std::string(*(*it)->hash);
			open->score = (*it)->score;
			open->manhattan = (*it)->manhattan;
			open->tab = (*it)->tab;	// must create a copy ?
			this->_opened->push_back(open);
			this->_closed->erase(it);
			return (true);
		}
		else if ((*it)->hash->compare(state) == 0 && score >= (*it)->score)
			return (false);
		it++;
	}
	return (false);
}

bool	Calc::check_open(std::string state, int score, std::string old) {
	std::list<t_open *>::iterator it = this->_opened->begin();
	std::list<t_open *>::iterator end = this->_opened->end();

	while (it != end) {
		if ((*it)->hash->compare(state) == 0 && score < (*it)->score) {
			add_from_list(state, old);
			(*it)->score = score;
			return (true);
		}
		else if ((*it)->hash->compare(state) == 0 && score >= (*it)->score)
			return (true);
		it++;
	}
	return (false);
}


void	Calc::next_state( void ) {
	int	x = 0, y = 0;

	get_pos(0, &x, &y);	// get '0' pos in actual_state
	this->_available = 0;
	if (x - 1 >= 0) { // LEFT
		copy_map(0);
		change_zero_pos(0, y, x, y, x - 1);
		this->_test[0].hash = new std::string(*this->serialize(this->_test[0].tab, this->_size));
		this->_test[0].use = true;
		this->_available++;
		this->totalWayOpen++;
	}
	else
		this->_test[0].use = false;
	if (x + 1 < this->_size) { // RIGHT
		copy_map(1);
		change_zero_pos(1, y, x, y, x + 1);
		this->_test[1].hash = new std::string(*this->serialize(this->_test[1].tab, this->_size));
		this->_test[1].use = true;
		this->_available++;
		this->totalWayOpen++;
	}
	else
		this->_test[1].use = false;
	if (y - 1 >= 0) { // TOP
		copy_map(2);
		change_zero_pos(2, y, x, y - 1, x);
		this->_test[2].hash = new std::string(*this->serialize(this->_test[2].tab, this->_size));
		this->_test[2].use = true;
		this->_available++;
		this->totalWayOpen++;
	}
	else
		this->_test[2].use = false;
	if (y + 1 < this->_size) { // BOT
		copy_map(3);
		change_zero_pos(3, y, x, y + 1, x);
		this->_test[3].hash = new std::string(*this->serialize(this->_test[3].tab, this->_size));
		this->_test[3].use = true;
		this->_available++;
		this->totalWayOpen++;
	}
	else
		this->_test[3].use = false;
}


bool	Calc::check_time( void ) {	// opti : check tout les X states checked
	time_t	timer;

	timer = time(NULL);
	if (timer - this->_time > 3 * this->_size)
		return (true);
	return (false);
}

int		Calc::next_move( void ) {
	std::list<t_open *>::iterator open;
	std::list<t_open *>::iterator open_end = this->_opened->end();
	std::list<t_open *>::iterator good_open;
	t_closed * closed = NULL;
	int cost = -1, i = 0;

	if (check_time() == true)
		return (-1);
	if (this->_opened->size() <= 0)
		return (-1); // must check if puzzle done
	open = this->_opened->begin();
	while (open != open_end) {	// test, near 70 % fast
		if ((*open)->manhattan < cost || cost < 0) {
			good_open = open;
			cost = (*open)->manhattan;
		}
		open++;
	}
	if (is_done(*good_open) == true) {
		this->w_log("call to fill solution");
		fill_solution();
	this->w_log("after fill solution");
		return (0); // solution found
	}
	closed = new_closed();
	// std::cout << "test1 " << (*good_open)->hash << std::endl;
	closed->hash = new std::string(*(*good_open)->hash);
	// std::cout << "test2 " << (*good_open)->score << std::endl;
	closed->score = (*good_open)->score;
	// std::cout << "test3 " << (*good_open)->manhattan << std::endl;
	closed->manhattan = (*good_open)->manhattan;
	// std::cout << "test4 " << std::endl;
	closed->tab = (*good_open)->tab;	// must create a copy ?
	// std::cout << "test5 " << std::endl;
	this->_closed->insert(closed);
	// std::cout << "test6 " << std::endl;
	init_actual_state((*good_open)->tab);
	next_state();
	while (i < 4) {
		if (this->_test[i].use == true && is_close(*this->_test[i].hash) == true) {
			this->_test[i].use = false;
			this->_available--;
		}
		i++;
	}
	if (this->_available > 0) {
		do_calc((*good_open)->score + 1, *(*good_open)->hash);
	}
	this->_opened->erase(good_open);
	return (1);
}

bool	Calc::run_calc( void ) {
	int ret = 0;

	this->totalWayOpen = 0;
	while ((ret = next_move()) > 0);
	if (ret < 0) {
		w_log("Event::run : no solution"); // must exit
		this->possibleSolution = false;
		return (false);
	}
	else {
		w_log("Event::run : FIND SOLUTION !");
		this->possibleSolution = true;
		return (true);
	}
}

void	Calc::delete_Solution(void) {
	if (this->_waySolution != NULL && this->_waySolution->size() > 0)
		this->_waySolution->erase (this->_waySolution->begin(), this->_waySolution->end());
	if (this->_actual_state != NULL)
		this->_actual_state->erase (this->_actual_state->begin(), this->_actual_state->end());
	if (this->_map_final != NULL)
		this->_map_final->erase (this->_map_final->begin(), this->_map_final->end());
}

void 	Calc::fill_solution(void) {
	std::string	*	state = new std::string(g_select[this->_size -3]);
	std::map<std::string, std::string>::iterator	IEnd = this->_from->end();
	std::map<std::string, std::string>::iterator	it;

    this->_waySolution = new std::list<std::string>();
    if (NULL == this->_waySolution) {
    	this->w_error("Calc::fill_solution Memory fail.");
		throw std::exception();
    }

    it = this->_from->find(*state);
    while (1) {
    	if ( it == IEnd ) {
    		break;
    	}
    	this->_waySolution->push_front(std::string(it->second));
    	it = this->_from->find(it->second);
    	
    }
    this->_opened->erase (this->_opened->begin(), this->_opened->end());
    this->_closed->erase (this->_closed->begin(), this->_closed->end());
    this->_from->erase (this->_from->begin(), this->_from->end());
}

void	Calc::print_debug_map_to_log(Map *tab) {
	std::vector<int>	vect;
	int 				j = 0, i;
	std::string			s;

	while (j < this->_size) {
		i = 0;
		vect = tab->find(j)->second;
		while (i < this->_size) {
			s = s + ' ' + std::to_string(vect[i]);
			i++;
		}
		this->w_log(s);
		s.erase();
		j++;
	}
	this->w_log("");
}
