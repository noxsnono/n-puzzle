#include <Parse.class.hpp>

Parse::Parse( char ** av ) : _map(NULL), _size(0), _mapY(0) {
	bool 				firstline = false;
	std::stringstream	ss;
	std::string 		buff;

	ss << av[1];
	this->_lodFD.open(ss.str(), std::ifstream::in);
	if (this->_lodFD.fail() == 1) {
		std::cerr << "Parse::Parse Open File" << std::endl;
		throw std::exception();
	}
	while (std::getline(this->_lodFD, buff).eof() == 0) {
		if ('#' == buff[0])
			;
		else if ('#' != buff[0] && false == firstline) {
			this->sizeLine(buff);
			firstline = true;
		}
		else {
			this->tabLine(buff);
		}
	}
}

void	Parse::tabLine( std::string const & line ) {
	int					i = 0;
	std::vector<int>	tabLine;
	char const *		charLine = line.c_str();

	while ('\0' != line[i]) {
		if ('#' == line[i])
			break ;
		else if (' ' == line[i]) {
			while ('\0' != '\0' && ' ' == line[i])
				i++;
		}
		else if ('\0' != line [i]) {
			this->_nbr.push_front(atoi(&charLine[i]));
			tabLine.push_back(atoi(&charLine[i]));
			while ('\0' != line[i] && true == isdigit(charLine[i]))
				i++;
		}
		if ('\0' != line[i])
			i++;
	}
	this->_map->insert(std::pair<int, std::vector<int>>(this->_mapY, tabLine));
	this->_mapY++;
}

void	Parse::sizeLine( std::string const	& line ) {
	this->_size = atoi(line.c_str());
	this->_map = new Map();
	if (NULL == this->_map) {
		this->w_error("Parse::sizeLine Memory Allocation error.");
		throw std::exception();
	}
}

Map *	Parse::getMap( void ) { return this->_map; }

int 	Parse::getSize( void ) { return this->_size; }

Parse::Parse() {}

Parse::~Parse() {}
