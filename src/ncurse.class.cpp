#include <ncurse.class.hpp>

Graphic::Graphic( Event * event ) :	_window(NULL),
									_key(NULL),
									FirstLaunch(true),
									mainEvent(event) {

	t_data *	d = get_data();

	if (NULL == d) {
		std::cerr << "Graphic::Graphic Assign Graphic ncurse prt error." << std::endl; 
		throw std::exception();
	}
	d->graphic = this;
}

void 	Graphic::setWaySolution( void ) {
	this->_waySolution = this->mainEvent->get_calc()->getWaySolution();
}

void	Graphic::init( int size, Map * map ) {
	this->_size = size;
	this->allCaseSize = (this->_size) * CASE_SIZE_Y;
	this->_map = map;
	this->_originalMap = map;
	this->_key = new std::vector<int>;
	initscr();
	this->screenY = this->_size * (CASE_SIZE_Y) + 26;
	this->screenX = this->_size * (CASE_SIZE_X) + 30;
	this->_window = newwin(this->screenY, this->screenX, 0, 0);
	if (this->_window == NULL) {
		this->w_error("Ncurses Window is NULL");
		throw std::exception();
	}
	cbreak();
	noecho();
	nodelay(stdscr, true);
	curs_set(0);
	keypad(this->_window, true);
	wrefresh(this->_window);
}

void	Graphic::close( void ) {
	wrefresh(this->_window);
	delwin(this->_window);
	endwin();
}

void	Graphic::keyboard( void ) {
	int				key,
					touch = 1;
	static t_data *	d = get_data();

	wtimeout(this->_window, 1);
	while (touch == 1)
	{
		key = wgetch(this->_window);
		if (key == 127 || key == 100) {
			this->w_log("Key touch SPACE || 'A'");
			if (this->_itWaySolution != this->_wayBegin) {
				this->_itWaySolution--;
				this->_map = this->toTab( &(*this->_itWaySolution), this->_size );
			}
		}
		else if (key == 32 || key == 97) {
			if (this->_itWaySolution != this->_wayEnd) {
				this->_itWaySolution++;
				if (this->_itWaySolution == this->_wayEnd)
					this->_map = this->toTab( &g_select[this->_size -3], this->_size );
				else
					this->_map = this->toTab( &(*this->_itWaySolution), this->_size );
			}
		}
		else if (key == 49) {
			this->w_log("Key touch 1");
			this->change_mode_way_solution(0);
		}
		else if (key == 50) {
			this->w_log("Key touch 2");
			this->change_mode_way_solution(1);
		}
		else if (key == 51) {
			this->w_log("Key touch 3");
			this->change_mode_way_solution(2);
		}
		else if (key == 52) {
			this->w_log("Key touch 4");
			this->change_mode_way_solution(3);
		}
		else if (key == 27 || key == 101 || key == 113) {
			d->event->running = false;
			this->w_log("Exit from Keyboard");
			return ;
		}
		else if (key == -1) {
			touch = 0;
		}
	}
}

void 	Graphic::change_mode_way_solution( int mode ) {
	this->FirstLaunch = false;
	if (this->mainEvent->get_calc() != NULL) {
		if (this->_waySolution != NULL && this->_waySolution->size() != 0) {
			this->_waySolution->erase (this->_waySolution->begin(), this->_waySolution->end());
			this->mainEvent->get_calc()->delete_Solution();
		}
		delete this->mainEvent->get_calc();
		this->mainEvent->set_new_calc();
	}
	this->mainEvent->get_calc()->change_calc_mode(mode);
	this->_time_start = time(NULL);
	this->mainEvent->get_calc()->run_calc();
	this->_time_end = time(NULL);
	if (this->mainEvent->get_calc()->possibleSolution == true) {
		this->setWaySolution();
		this->_wayBegin = this->_waySolution->begin();
		this->_wayEnd = this->_waySolution->end();
		this->_itWaySolution = this->_waySolution->begin();
	}
	else {
		this->_map = this->_originalMap;
	}
}

void	Graphic::render_scene( void ) {
	int					x,
						y;
	std::vector<int>	vect;

	keyboard();
	werase(this->_window);
	wborder(this->_window, '+', '+', '+', '+', 0, 0, 0, 0);
	this->draw_legend();
	for (y = 0; y < this->_size; y++) {
		vect = this->_map->find(y)->second;
		for (x = 0; x < this->_size; x++) {
			this->draw_square(y, x, vect[x]);
		}
	}
	wrefresh(this->_window);
}

void	Graphic::draw_legend( void ) {
	std::stringstream	ss;

	//"'SPACE' for next step"
	mvwprintw(this->_window, this->allCaseSize + LEGEND_SPACE_Y, 3, "%s", LEGEND_SPACE);
	//"'DELETE' or previous step"
	mvwprintw(this->_window, this->allCaseSize + LEGEND_DELETE_Y, 3, "%s", LEGEND_DELETE);
	//"'ECHAP' 'Q' 'E' to exit program"
	mvwprintw(this->_window, this->allCaseSize + LEGEND_QUIT_Y, 3, "%s", LEGEND_QUIT);
	//"Jump to solution:
	mvwprintw(this->_window, this->allCaseSize + JUMP_TO_SOLUTION_Y, 3, "%s", JUMP_TO_SOLUTION);
	//"Total Ways opened:
	mvwprintw(this->_window, this->allCaseSize + TOTAL_OPEN_WAY_Y, 3, "%s", TOTAL_OPEN_WAY);
	//"Resolution's time in seconds:"
	mvwprintw(this->_window, this->allCaseSize + TIME_Y, 3, "%s", TIME);
	if (this->mainEvent->get_calc()->possibleSolution == true) {
		ss << this->_waySolution->size();
		//"Jump to solution: Value
		mvwprintw(this->_window, this->allCaseSize + JUMP_TO_SOLUTION_Y, 22, "%s", ss.str().c_str());
		ss.str("");
		//"Total Ways opened: Value
		ss << this->mainEvent->get_calc()->totalWayOpen;
		mvwprintw(this->_window, this->allCaseSize + TOTAL_OPEN_WAY_Y, 22, "%s", ss.str().c_str());
		ss.str("");
		ss << ( this->_time_end - this->_time_start );
		//"Resolution's time in seconds: VALUE"
		mvwprintw(this->_window, this->allCaseSize + TIME_Y, 33, "%.6s seconds", ss.str().c_str());
	}
	else {
		mvwprintw(this->_window, this->allCaseSize + JUMP_TO_SOLUTION_Y, 22, "%s", "0");
		mvwprintw(this->_window, this->allCaseSize + TOTAL_OPEN_WAY_Y, 22, "%s", "0");
	}
	// method manatahn
	mvwprintw(this->_window, this->allCaseSize + MODE_MANATHAN_Y, 3, "%s", MODE_MANATHAN);
	// good cases
	mvwprintw(this->_window, this->allCaseSize + MODE_GOOD_CASES_Y, 3, "%s", MODE_GOOD_CASES);
	// wrong cases
	mvwprintw(this->_window, this->allCaseSize + MODE_WRONG_CASES_Y, 3, "%s", MODE_WRONG_CASES);
	// Bounded Relaxation
	mvwprintw(this->_window, this->allCaseSize + MODE_BOUNDED_CASES_Y, 3, "%s", MODE_BOUNDED_CASES);
	//"Possible Solution" or "Impossible Solution"
	if (this->mainEvent->get_calc()->possibleSolution == true)
		mvwprintw(this->_window, this->allCaseSize + SOLUTION_Y, 3, "%s", SOLUTION_YES);
	else if (this->FirstLaunch == true){
		mvwprintw(this->_window, this->allCaseSize + SOLUTION_Y, 3, "%s", VOID_STRING);
	}
	else
		mvwprintw(this->_window, this->allCaseSize + SOLUTION_Y, 3, "%s", SOLUTION_NO);
}

void	Graphic::draw_square( int IncY, int IncX, int nbr ) {
	int y, x;

	y = 0;
	this->draw_border_square(IncY * CASE_SIZE_Y, IncX * CASE_SIZE_X);
	while (y < this->_size) {
		x = 0;
		while (x < this->_size) {
			if (nbr != 0)
				mvwprintw(this->_window, IncY * CASE_SIZE_Y + 2, IncX * CASE_SIZE_X + 3, "%d", nbr);
			else
				this->draw_void(IncY, IncX);
			x++;
		}
		y++;
	}
}

void 	Graphic::draw_void( int IncY, int IncX) {
	int y;

	y = 1;
	while (y < CASE_SIZE_Y - 1) {
		mvwprintw(this->_window, IncY * CASE_SIZE_Y + y, IncX * CASE_SIZE_X + 1, "######");
		y++;
	}
}

void	Graphic::draw_border_square(int IncY, int IncX) {
	int y, x;

	y = 0;
	while (y < CASE_SIZE_Y) {
		x = 0;
		while (x < CASE_SIZE_X) {
			if (x == 0 || y == 0 || x == CASE_SIZE_X - 1 || y == CASE_SIZE_Y - 1)
				mvwprintw(this->_window, y + IncY, x + IncX, "%s", "+");
			x++;
		}
		y++;
	}
}

Graphic & Graphic::operator=( Graphic const & rhs ) {
	if (this != &rhs) {}
	return (*this);
}

Graphic::Graphic( Graphic const & src ) { *this = src; }

Graphic::~Graphic( void ) {}

void 	Graphic::setMap( Map * map ) { this->_map = map; }

std::vector<int> **Graphic::get_touch_list( void ) { return (&this->_key); }
