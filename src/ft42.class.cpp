#include <ft42.class.hpp>

std::string		ft42::logFileName;
std::ofstream	ft42::lodFD;

Map *		ft42::toTab( std::string const * inc, int size ) {
	int		y = 0,
			i = 0,
			pos = 0;
	Map		*tab = new Map();
 	std::vector<int> line;

	while (y < size) {
		i = 0;
		while (i < size) {
			line.push_back(inc->at(pos) - 33);
			pos++;
			i++;
		}
		tab->insert(std::pair<int,std::vector<int>>(y,line));
		line.clear();
		y++;
	}
	return tab;
}

std::string	*	ft42::serialize( Map *map, int size ) {
	std::vector<int>			vect;
	int		x,
			y = 0,
			pos = 0;
	char	buff[(size * size) + 1];

	bzero(buff, (size * size) + 1);
	while (y < size) {
		x = 0;
		vect = map->find(y)->second;
		while (x < size) {
			buff[pos] = vect[x] + 33;
			x++;
			pos++;
		}
		y++;
	}
	buff[pos] = '\0';
	std::string	*	result = new std::string(buff);
	return result;
}

void			ft42::w_full( std::string const newEntry ) {
	std::cout << newEntry << std::endl;
	ft42::lodFD << ft42::logTime( newEntry ) << std::endl;
}

void			ft42::w_error( std::string const newEntry ) {
	std::cerr << newEntry << std::endl;
	
	this->w_log(newEntry);
}

void			ft42::w_log( std::string const newEntry ) {
	if ( ft42::logFileName.empty() && ft42::lodFD.is_open() == 0) {
		std::stringstream	ss;
		ss << LOG_PATH;
		ft42::lodFD.open(ss.str(), std::ofstream::out | std::ofstream::app);
		this->lodFD << std::endl << std::endl << std::endl << std::endl << std::endl;
		if (this->lodFD.fail() == 1) {
			std::cerr << "Log File Open error" << std::endl;
			throw std::exception();
		}
	}
	this->lodFD << ft42::logTime( newEntry ) << std::endl;
}

std::string		ft42::logTime( std::string const & sentence ) {
	std::stringstream	ss;

	memset(&this->buffer, 0, 80);
	time(&this->timer);
	this->timeinfo = localtime (&this->timer);
	strftime (this->buffer, 80, "%Y/%m/%d %H:%M:%S ", this->timeinfo);
	ss << this->buffer;
	this->result = ss.str() + sentence;
	return (this->result);
}

std::ofstream &	ft42::getLogFD( void ) { return ( ft42::lodFD ); }

ft42::ft42() {}

ft42::~ft42() {}
